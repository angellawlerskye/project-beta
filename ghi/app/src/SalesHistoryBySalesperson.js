import React, { useEffect, useState } from 'react';

function SalesHistoryBySalesperson() {
  const [salespeople, setSalespeople] = useState([]);
  const [salesperson, setSalesperson] = useState('');
  const [sales, setSales] = useState([]);

  const handleSalespersonChange = (event) => {
    setSalesperson(event.target.value);
  };

  const fetchSalespeople = async () => {
    const SalespeopleResponse = await fetch('http://localhost:8090/api/salespeople/');
    if (SalespeopleResponse.ok) {
      const sdata = await SalespeopleResponse.json();
      setSalespeople(sdata.salespeople);
    }
  };

  useEffect(() => {
    fetchSalespeople();
  }, []);

  useEffect(() => {
    if (salesperson) {
      const getSales = async () => {
        let saleUrl = `http://localhost:8090/api/salesperson/${salesperson}/sales`;
        const response = await fetch(saleUrl);
        if (response.ok) {
          const data = await response.json();
          setSales(data.sales);
        }
      };
      getSales();
    } else {
      setSales([]);
    }
  }, [salesperson]);

  return (
    <div className="row">
      <div className="offset-1 col-9">
        <div className="shadow p-4 mt-4">
          <h3>Sales History by Salesperson</h3>
          <select
            onChange={handleSalespersonChange}
            required
            name="salesperson"
            id="salesperson"
            className="form-select"
            value={salesperson}
          >
            <option value="">Select a Salesperson</option>
            {salespeople.map((sperson) => (
              <option key={sperson.id} value={sperson.id}>
                {sperson.first_name} {sperson.last_name}
              </option>
            ))}
          </select>
          <div className="table-responsive">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Salesperson</th>
                  <th>Customer</th>
                  <th>Automobile VIN</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                {sales.map((sale) => (
                  <tr key={sale.id}>
                    <td>
                      {sale.salesperson.first_name} {sale.salesperson.last_name}
                    </td>
                    <td>
                      {sale.customer.first_name} {sale.customer.last_name}
                    </td>
                    <td>{sale.automobile.vin}</td>
                    <td>${sale.price.toFixed(2)}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalesHistoryBySalesperson;