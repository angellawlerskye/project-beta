import React, { useState, useEffect } from 'react';

function SalespeopleList() {
  const [salespeople, setSalespeople] = useState([]);

  useEffect(() => {
    // Simulate an API call to fetch salespeople data.
    // Replace this with your actual data fetching logic.
    const fetchData = async () => {
      try {
        // Fetch data from your API or data source.
        const response = await fetch('http://localhost:8090/api/salespeople/'); // Update the API endpoint.
        const data = await response.json();
        setSalespeople(data.salespeople);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []); // The empty dependency array ensures that this effect runs once on component mount.

  return (
    <div>
      <h2>Salespeople</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map((salesperson) => (
            <tr key={salesperson.id}>
              <td>{salesperson.first_name}</td>
              <td>{salesperson.last_name}</td>
              <td>{salesperson.employee_id}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalespeopleList;