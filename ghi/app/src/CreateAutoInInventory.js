import React, { useEffect, useState } from "react";

function CreateAutoInInventory(){
    const [model, setModels] = useState([]);
    const [year, setYear] = useState("");
    const [vin, setVin] = useState("");
    const [color, setColor] = useState("");
    const [model_id, setModelId] = useState("");

    const handleChangeYear = (event) => {
        const value = event.target.value;
        setYear(value);
    };

    const handleChangeVin = (event) => {
        const value = event.target.value;
        setVin(value);
    };

    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    };

    const handleChangeModelId = (event) => {
        const value = event.target.value;
        setModelId(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            color: color,
            year: year,
            vin: vin,
            model_id: model_id,
        };

    const CreateAutoInInventoryUrl = "http://localhost:8100/api/automobiles/";
    const fetchOptions = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
        },
    };

    try {
        const response = await fetch(CreateAutoInInventoryUrl, fetchOptions);
        if (response.ok) {
            setColor("");
            setYear("");
            setVin("");
            setModelId("");
        }
    }catch (error) {
        console.error(error);
    }
   };

   const fetchData = async () => {
    const modelsURL = "http://localhost:8100/api/models/";
    try{
        const response = await fetch(modelsURL);
        if (response.ok){
            const data = await response.json();
            setModels(data.models);
        }
    }catch (error) {
        console.error(error);
    }
   };

   useEffect(() => {
    fetchData();
   }, [])

   return (
    <div>
      <h2>Create Automobile in Inventory</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Year:
          <input type="text" id="year" name="year" value={year} onChange={handleChangeYear} />
          </label>
        </div>
        <div>
          <label>VIN:
          <input type="text" id="vin" name="vin" value={vin} onChange={handleChangeVin} />
          </label>
        </div>
        <div>
          <label>Color:
          <input type="text" id="color" name="color" value={color} onChange={handleChangeColor} />
          </label>
        </div>
        <div>
        <label>Model:
            <select id="model_id" name="model_id" value={model_id} onChange={handleChangeModelId}>
                <option value="">Select a Model</option>
                {Array.isArray(model) && model.length > 0 ? (
            model.map((model) => (
        <option key={model.id} value={model.id}>
          {model.name}
        </option>
      ))
    ) : (
      <option value="">Loading or no data available</option>
    )}
  </select>
  </label>
</div>
        <button type="submit">Create Automobile</button>
      </form>
    </div>
  );
}

export default CreateAutoInInventory;
