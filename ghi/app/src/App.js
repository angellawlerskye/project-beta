import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import ModelsList from './ModelsList';
import SalespeopleList from './SalespeopleList';
import CustomerList from './CustomerList';
import CreateSalespersonForm from './CreateSalespersonForm';
import CreateCustomerForm from './CreateCustomerForm';
import AddTechnician from './AddTechnician';
import ListOfTechnician from './ListTechnicians';
import ServiceAppointment from './CreateServiceAppointment';
import AppointmentsList from './ListServiceAppointments';
import ServiceHistory from './ServiceHistory';
import CreateVehicleModel from './CreateVehicleModel';
import ListAutoInInventory from './ListOfAutoInInventory';
import CreateAutoInInventory from './CreateAutoInInventory';
import NewSaleRecordForm from './NewSaleRecordForm';
import SalesRecord from './SalesRecord';
import SalesHistoryBySalesperson from './SalesHistoryBySalesperson';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers" element={<ManufacturersList />} />
          <Route path="manufacturers/create" element={<ManufacturerForm />} />
          <Route path="models" element={<ModelsList />} />
          <Route path="salespeople" element={<SalespeopleList />} />
          <Route path="salespeople/create" element={<CreateSalespersonForm />} />
          <Route path="customers" element={<CustomerList />} />
          <Route path="customers/create" element={<CreateCustomerForm />} />
          <Route path="api/technicians/create" element={<AddTechnician />} />
          <Route path="api/technicians" element={<ListOfTechnician />} />
          <Route path="api/appointments/create" element={<ServiceAppointment />} />
          <Route path="api/appointments" element={<AppointmentsList />} />
          <Route path="api/service/history" element={<ServiceHistory />} />
          <Route path="api/models" element={<CreateVehicleModel />} />
          <Route path="api/automobiles" element={<ListAutoInInventory />} />
          <Route path="api/automobiles/create" element={<CreateAutoInInventory />} />
          <Route path="sales" element={<SalesRecord />} />
          <Route path="sales/history" element={<SalesHistoryBySalesperson />} />
          <Route path="sales/create" element={<NewSaleRecordForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
