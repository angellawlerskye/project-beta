import React, { useState, useEffect } from 'react';

function CustomersList() {
  const [customers, setCustomers] = useState([]);

  useEffect(() => {
    // Simulate an API call to fetch customers data.
    // Replace this with your actual data fetching logic.
    const fetchData = async () => {
      try {
        // Fetch data from your API or data source.
        const response = await fetch('http://localhost:8090/api/customers/'); // Update the API endpoint.
        const data = await response.json();
        setCustomers(data.customers);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []); // The empty dependency array ensures that this effect runs once on component mount.

  return (
    <div>
      <h2>Customers</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>Phone Number</th>
          </tr>
        </thead>
        <tbody>
          {customers.map((customer) => (
            <tr key={customer.id}>
              <td>{customer.first_name}</td>
              <td>{customer.last_name}</td>
              <td>{customer.address}</td>
              <td>{customer.phone_number}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default CustomersList;