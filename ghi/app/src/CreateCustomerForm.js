import React, { useState } from 'react';

function CreateCustomerForm() {
  const [newCustomer, setNewCustomer] = useState({
    first_name: '',
    last_name: '',
    address: '',
    phone_number: '',
  });

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch('http://localhost:8090/api/customers/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newCustomer),
      });

      if (response.status === 200) {
        // Customer created successfully.
        // Reset the form
        setNewCustomer({
          first_name: '',
          last_name: '',
          address: '',
          phone_number: '',
        });
      }
    } catch (error) {
      // Handle network errors, e.g., show a network error message.
      console.error('Network error: ' + error.message);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setNewCustomer({ ...newCustomer, [name]: value });
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-6 offset-md-3">
          <div className="card mt-4">
            <div className="card-header">
              <h2 className="card-title">Add A Customer</h2>
            </div>
            <div className="card-body">
              <form onSubmit={handleSubmit}>
                <div className="mb-3">
                  <label className="form-label">First Name:</label>
                  <input
                    type="text"
                    name="first_name"
                    value={newCustomer.first_name}
                    onChange={handleChange}
                    className="form-control"
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">Last Name:</label>
                  <input
                    type="text"
                    name="last_name"
                    value={newCustomer.last_name}
                    onChange={handleChange}
                    className="form-control"
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">Address:</label>
                  <input
                    type="text"
                    name="address"
                    value={newCustomer.address}
                    onChange={handleChange}
                    className="form-control"
                  />
                </div>
                <div className="mb-3">
                <label className="form-label">Phone Number</label>
<input
  type="text"
  name="phone_number"
  value={newCustomer.phone_number}
  onChange={handleChange}
  className="form-control"
  placeholder="5050101"
  pattern="\d{7}" // Use the 'pattern' attribute to enforce 7 digits
  title="Please enter a 7-digit phone number without hyphens " // Optional title attribute for custom error message
/>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateCustomerForm;