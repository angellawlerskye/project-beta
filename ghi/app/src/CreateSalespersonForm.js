import React, { useState } from 'react';

function CreateSalespersonForm() {
  const [newSalesperson, setNewSalesperson] = useState({
    first_name: '',
    last_name: '',
    employee_id: '',
  });

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch('http://localhost:8090/api/salespeople/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newSalesperson),
      });

      if (response.status === 200) {
        // Salesperson created successfully.
        // Reset the form
        setNewSalesperson({
          first_name: '',
          last_name: '',
          employee_id: '',
        });
      }
    } catch (error) {
      // Handle network errors, e.g., show a network error message.
      console.error('Network error: ' + error.message);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setNewSalesperson({ ...newSalesperson, [name]: value });
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-6 offset-md-3">
          <div className="card mt-4">
            <div className="card-header">
              <h2 className="card-title">Add A Salesperson</h2>
            </div>
            <div className="card-body">
              <form onSubmit={handleSubmit}>
                <div className="mb-3">
                  <label className="form-label">First Name:</label>
                  <input
                    type="text"
                    name="first_name"
                    value={newSalesperson.first_name}
                    onChange={handleChange}
                    className="form-control"
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">Last Name:</label>
                  <input
                    type="text"
                    name="last_name"
                    value={newSalesperson.last_name}
                    onChange={handleChange}
                    className="form-control"
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">Employee ID:</label>
                  <input
                    type="text"
                    name="employee_id"
                    value={newSalesperson.employee_id}
                    onChange={handleChange}
                    className="form-control"
                  />
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateSalespersonForm;