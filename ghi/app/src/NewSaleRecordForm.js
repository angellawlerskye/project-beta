import React, { useEffect, useState } from 'react';

function NewSaleRecordForm() {
    const [autos, setAutos] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [auto, setAuto] = useState('');
    const [salesperson, setSalesperson] = useState('');
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState('');

    const handleAutoChange = (event) => { setAuto(event.target.value); };
    const handleSalespersonChange = (event) => { setSalesperson(event.target.value); };
    const handleCustomerChange = (event) => { setCustomer(event.target.value); };
    const handlePriceChange = (event) => { setPrice(event.target.value); };

    const handleSend = async (event) => {
        event.preventDefault();
        const data = {};
        data.automobile = auto;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;

        const SaleUrl = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(SaleUrl, fetchConfig);
        if (response.ok) {
            setAuto('');
            setSalesperson('');
            setCustomer('');
            setPrice('');
        }
    }

    const fetchData = async () => {
        const AutoResponse = await fetch('http://localhost:8100/api/automobiles/');
        const SalespeopleResponse = await fetch('http://localhost:8090/api/salespeople/');
        const CustomerResponse = await fetch('http://localhost:8090/api/customers/');
        if (AutoResponse.ok && SalespeopleResponse.ok && CustomerResponse.ok) {
            const adata = await AutoResponse.json();
            const sdata = await SalespeopleResponse.json();
            const cdata = await CustomerResponse.json();

            setAutos(adata.autos);
            setSalespeople(sdata.salespeople);
            setCustomers(cdata.customers);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Sale</h1>
                    <form onSubmit={handleSend} id="create-sale-form">
                        <div className="mb-3">
                            <label htmlFor="auto">Automobile VIN</label>
                            <select onChange={handleAutoChange} required name="auto" id="auto" className="form-select" value={auto}>
                                <option value="">Select</option>
                                {autos.map(auto => (
                                    <option key={auto.id} value={auto.vin}>
                                        {auto.vin}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="salesperson">Salesperson</label>
                            <select onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select" value={salesperson}>
                                <option value="">Select</option>
                                {salespeople.map(salesperson => (
                                    <option key={salesperson.id} value={salesperson.id}>
                                        {salesperson.first_name} {salesperson.last_name}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="customer">Customer</label>
                            <select onChange={handleCustomerChange} required name="customer" id="customer" className="form-select" value={customer}>
                                <option value="">Select</option>
                                {customers.map(customer => (
                                    <option key={customer.id} value={customer.id}>
                                        {customer.first_name} {customer.last_name}
                                    </option>
                                ))}
                            </select>
                        </div><label htmlFor="price">Price</label>
                        <div className="form mb-1">
                            <input onChange={handlePriceChange} placeholder="Please enter a number with no commas or dollar signs." required type="number" name="price" id="price" className="form-control" value={price} />
                        </div>
                        <button className="btn btn-primary">Create New Sale Record</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default NewSaleRecordForm;