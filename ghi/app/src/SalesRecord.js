import React, { useState, useEffect } from 'react';

function SalesList() {
  const [sales, setSales] = useState([]);

  useEffect(() => {
    // Fetch the list of sales from your API
    const fetchSales = async () => {
      try {
        const response = await fetch('http://localhost:8090/api/sales/');
        const data = await response.json();
        setSales(data.sales);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchSales();
  }, []);

  return (
    <div>
      <h2>Sales List</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Employee ID</th>
            <th>Customer</th>
            <th>Automobile VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale) => (
            <tr key={sale.id}>
              <td>
                {sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                <td> {sale.salesperson.employee_id}
              </td>
              <td>{sale.customer.first_name} {sale.customer.last_name}</td>
              <td>{sale.automobile.vin}</td>
              <td>${sale.price.toFixed(2)}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalesList;