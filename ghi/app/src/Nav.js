import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">FleetFolio</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/create">Create A Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models">Models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople">Salespeople</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople/create">Create A Salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales">Sales Record</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales/history">Sales Record By Salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales/create">Create A New Sales Record</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers">Customers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers/create">Create A Customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/api/technicians/create"> Create A Technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/api/technicians"> List Of Technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/api/appointments/create"> Create Service Appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/api/appointments">List Of Serivce Appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/api/service/history"> Service History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/api/models/"> Create Vehicle Model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/api/automobiles/">List Of Automobiles In Inventory</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/api/automobiles/create">Create Automobile In Inventory</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;