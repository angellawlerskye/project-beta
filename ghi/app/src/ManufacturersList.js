import React, { useState, useEffect } from 'react';

function ManufacturersList() {
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    // Simulate an API call to fetch manufacturers data.
    // Replace this with your actual data fetching logic.
    const fetchData = async () => {
      try {
        // Fetch data from your API or data source.
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        const data = await response.json();
        setManufacturers(data.manufacturers);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []); // The empty dependency array ensures that this effect runs once on component mount.

  return (
    <div>
      <h2>Manufacturers</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map((manufacturer) => (
            <tr key={manufacturer.id}>
              <td>{manufacturer.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ManufacturersList;