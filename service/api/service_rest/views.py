from typing import Any
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from django.core.serializers.json import DjangoJSONEncoder
from datetime import time

from .models import AutomobileVO, Technician, Appointment

class TimeEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj,time):
            return obj.strftime("%H:%M:%S")
        elif obj is None:
            return None
        elif isinstance(obj,str):
            return obj
        return super().default(obj)


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "id",
        "sold"
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer", 
        "technician",
        "id",
        "vip",
        "time"
        
    ]
    encoders = {
        "technician": TechnicianEncoder(),
        "time": TimeEncoder(),
    }

# LIST OF TECHS, CREATING A TECH, DELETE A TECH-----------------------------------
@require_http_methods(["GET", "POST", "DELETE"])
def api_list_technicians(request, id=None):
    if request.method == "GET":
        technician = Technician.objects.all()
        if not technician:
            return JsonResponse(
                {"message": "No technician found sorry"},
                status=404,
            )
        return JsonResponse(
            {"technicians": technician},
            encoder=TechnicianEncoder,
        )
# CREATING A TECH------------------------------------------------------ 
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe = False,
            )
        except json.JSONDecodeError:
            return JsonResponse(
                {"message": "Data invaild"},
                status=400
            )
#DELETE A TECH------------------------------------------------------ 
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=id)
            technician.delete()
            return JsonResponse({"deleted": True})
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician not found sorry"},
                status=404,
            )


# LIST OF APP AD CREATING AN APP DELETING AN APP---------------------
@require_http_methods(["GET", "POST", "PUT", "DELETE"])
def api_list_appointments(request, id=None, vin=False):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        if not appointment:
            return JsonResponse(
                {"message": "No appointments sorry"},
                status=404,
            )
        return JsonResponse(
            {"appointments": appointment},
            encoder=AppointmentEncoder,
        )
# UPDATE A APP---------------------------------------------------
    elif request.method == "PUT":
        try:
            content=json.loads(request.body)
            appointment = Appointment.objects.get(id=id)
            props = ["status"]
            for prop in props:
                if prop in content:
                    setattr(appointment, prop, content[prop])
                appointment.save()
                return JsonResponse(
                    appointment,
                    encoder=AppointmentEncoder,
                    safe=False,
                )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appiontment does not exist"}),
            response.status_code = 404,
            return response
    
    elif request.method == "DELETE":
         count, _ = Appointment.objects.filter(id=id).delete()
         return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            technician = content.get("technician")
            technician = Technician.objects.get(id=technician)
            technician = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"messasge": "Invaild tech id"},
                status=400,
            )
        content["technician"] = technician
        try:
            vin = content["vin"]
            vip = AutomobileVO.objects.get(vin=vin)
            if vip:
                print("it is working")
                content["vip"] = True
                appointment = Appointment.objects.create(**content)
        except AutomobileVO.DoesNotExist:
            print("Auto does not work")
            appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
