from django.urls import path
from .views import(
    api_list_salespeople,
    api_delete_salesperson,
    api_list_customers,
    api_delete_customer,
    api_list_AutomobileVOs,
    api_new_sale_record,
    api_sales_by_salesperson,
)




urlpatterns = [
    path(
        "salespeople/",
        api_list_salespeople,
        name="api_list_salespeople"
    ),

    path(
        "salespeople/<int:salesperson_id>/",
        api_delete_salesperson,
        name="api_delete_salesperson"),

    path(
        "customers/",
        api_list_customers,
        name="api_list_customers"
    ),

    path(
        "customers/<int:customer_id>/",
        api_delete_customer,
        name="api_delete_customer"),

    path(
        "automobilevos/",
        api_list_AutomobileVOs,
        name="api_list_AutomobileVOs"),

    path(
        "sales/",
        api_new_sale_record,
        name="api_new_sale_record"),

    path(
        "salesperson/<int:id>/sales/",
        api_sales_by_salesperson,
        name="api_sales_by_salesperson"),
]