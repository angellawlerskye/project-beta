from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
import requests

from .encoders import (
    AutomobileVOEncoder,
    SalespersonEncoder,
    CustomerEncoder,
    SaleEncoder,
)
from .models import AutomobileVO, Salesperson, Customer, Sale

@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            content["salesperson"] = salesperson
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the salesperson"}
            )
            response.status_code = 400
            return response




@require_http_methods(["DELETE"])
def api_delete_salesperson(request, salesperson_id):
    try:
        # Retrieve the Salesperson instance to be deleted by their id number
        # Make sure to send the request to the proper api enpoint in
        # insomnia. If you want to delete a salesperson with the id of 4, the url needs to end with 4.
        # That's the id that's created automatically when you make a salesperson, NOT their "employee_id"
        salesperson = Salesperson.objects.get(id=salesperson_id)

        # Delete the Salesperson instance
        salesperson.delete()

        return JsonResponse(
            {"message": "Salesperson deleted successfully"},
        )

    except Salesperson.DoesNotExist:
        response = JsonResponse(
            {"message": "Salesperson not found"},
        )
        response.status_code = 404
        return response

    except Exception as e:
        response = JsonResponse(
            {"message": "Could not delete the salesperson", "error": str(e)},
        )
        response.status_code = 500
        return response



@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder  # Use your CustomerEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            content["customer"] = customer
            return JsonResponse(
                content,
                encoder=CustomerEncoder,  # Use your CustomerEncoder
                safe=False,
                status=201  # 201 Created status for successful POST
            )
        except ValueError as e:
            # Handle JSON decoding errors and provide details
            response = JsonResponse(
                {"error": "Invalid JSON data. Error in the following value: " + str(e)}
            )
            response.status_code = 400  # 400 Bad Request
            return response
        except ValidationError as e:
            # Handle validation errors and provide details of missing fields
            missing_fields = ", ".join(e.message_dict.keys())
            response = JsonResponse(
                {"error": "Validation error. Missing fields: " + missing_fields}
            )
            response.status_code = 400  # 400 Bad Request
            return response



@require_http_methods(["DELETE"])
def api_delete_customer(request, customer_id):
    try:
        # Retrieve the Customer instance to be deleted by their ID
        customer = Customer.objects.get(id=customer_id)

        # Delete the Customer instance
        customer.delete()

        return JsonResponse(
            {"message": "Customer deleted successfully"},
        )

    except Customer.DoesNotExist:
        response = JsonResponse(
            {"message": "Customer not found"},
        )
        response.status_code = 404
        return response

    except Exception as e:
        response = JsonResponse(
            {"message": "Could not delete the customer", "error": str(e)},
        )
        response.status_code = 400
        return response



@require_http_methods(["GET", "DELETE"])
def api_list_AutomobileVOs(request):
    if request.method == "GET":
        AutomobileVOs = AutomobileVO.objects.all()
        return JsonResponse(
            {"AutomobileVOs": AutomobileVOs},
            encoder=AutomobileVOEncoder,
        )
    else:
        AutomobileVOs = AutomobileVO.objects.all()
        AutomobileVOs.delete()
        return JsonResponse({"Message": "All AutomobileVOs Have Been Deleted"})


@require_http_methods(["GET", "POST"])
def api_new_sale_record(request):
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Invalid Salesperson ID"})

        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Invalid Customer ID"})

        try:
            auto_vin = content["automobile"]
            auto = AutomobileVO.objects.get(vin=auto_vin)
            print("auto=", auto)
            setattr(auto, "sold", True)
            auto.save()
            content["automobile"] = auto
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Invalid Automobile VIN"})

        sale = Sale.objects.create(**content)
        print("url=", f"http://project-beta-inventory-api-1:8000/api/automobiles/{auto_vin}/" )
        requests.put(
            f"http://project-beta-inventory-api-1:8000/api/automobiles/{auto_vin}/",
            data=json.dumps({"sold": "True"})
        )

        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False
        )
    else:
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder
        )


@require_http_methods("GET")
def api_sales_by_salesperson(request, id):
    salesperson = Salesperson.objects.get(id=id)
    sales = Sale.objects.get(salesperson_id=salesperson)
    return JsonResponse(
        {"sales": [sales]},
        encoder=SaleEncoder
    )