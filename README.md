# FleetFolio

The premiere solution for automobile dealership management!

## About Our Team

 Gevork Kvryan - Automobile Service Microservice

 Angel Lawler Skye - Automobile Sales Microservice




[Add Angel on LinkedIN!](https://www.linkedin.com/in/angel-lawler-skye/)

[Add Gevork on LinkedIN!](https://www.linkedin.com/in/gevork-kvryan-b19151277/)


## FleetFolio was created and tested with:


[![Docker](https://img.shields.io/badge/Docker-<COLOR>?style=for-the-badge&logo=docker)](https://www.docker.com/)
[![React](https://img.shields.io/badge/React-<COLOR>?style=for-the-badge&logo=react)](https://reactjs.org/)
[![Git](https://img.shields.io/badge/Git-<COLOR>?style=for-the-badge&logo=git)](https://git-scm.com/)
[![Node.js](https://img.shields.io/badge/Node.js-<COLOR>?style=for-the-badge&logo=node.js)](https://nodejs.org/)
[![Django](https://img.shields.io/badge/Django-<COLOR>?style=for-the-badge&logo=django)](https://www.djangoproject.com/)
[![Python](https://img.shields.io/badge/Python-<COLOR>?style=for-the-badge&logo=python)](https://www.python.org/)
[![JavaScript](https://img.shields.io/badge/JavaScript-<COLOR>?style=for-the-badge&logo=javascript)](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
[![JSON](https://img.shields.io/badge/JSON-<COLOR>?style=for-the-badge&logo=json)](https://www.json.org/)
[![JSX](https://img.shields.io/badge/JSX-<COLOR>?style=for-the-badge&logo=react)](https://reactjs.org/docs/introducing-jsx.html)
[![HTML](https://img.shields.io/badge/HTML-<COLOR>?style=for-the-badge&logo=html5)](https://developer.mozilla.org/en-US/docs/Web/HTML)
[![CSS](https://img.shields.io/badge/CSS-<COLOR>?style=for-the-badge&logo=css3)](https://developer.mozilla.org/en-US/docs/Web/CSS)
[![Insomnia](https://img.shields.io/badge/Insomnia-<COLOR>?style=for-the-badge&logo=insomnia)](https://insomnia.rest/)

## How to run FleetFolio:

You will need:
Docker, Git, and Node.js 18.2 or above.

You can click on the badges above if you need to download any of the above.

Then,

* fork and clone our repository.
* from the root directory of the project, run the following commands, one-at-a-time:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
Once your Docker Containers are running, you can view FleetFolio in your browser at:

http://localhost:3000

Use the hamburger menu to explore as you like.

### There will not be any data to sandbox with until you create some! Here's a few ways you can do that.**

You could, in this order:

-Use the Create A Manufacturer form to add a few vehichle manufacturers to populate a database.

-Use the Create A Model form to add a few vehicle models.

-Use the Create An Automobile form to add a few automobiles.

Once your local database contains a few automobiles, you will be able to make use of the full functionality of FleetFolio.


## **Import JSON File into Insomnia:**

If you'd like to quickly interface with our Endpoints without using our Front-End, you can use this [JSON file](https://gitlab.com/angellawlerskye/project-beta/-/blob/angel-dev/Project-Beta-Endpoints-Insomnia-.json) to import our entire collection of Endpoints into your Insomnia Workspace.

This file contains a collection of API endpoints and requests for our project.

***There is additional documentation defining FleetFolio's URLs, API Endpoints, and Testing info further down this page.***

## Design

FleetFolio consists of three back-end microservices, Inventory, Sales, and Service that interact with each other to manage our company's Inventory, Sales, and Service departments.


## Inventory Microservice

Our Inventory microservice contains three models.

- The Manufacturer model tracks a list of Vehicle Manufacturers
- The Models tracks a list of Vehicle Models by their manufacturer name, model name, and a photo.
- The Automobiles model tracks our Inventory by each automobile's manufacturer, model, and unique attributes such as VIN, model year, and color.

## Sales Microservice

Our Sales Microservice consists of four models.

- A Salesperson model that contains the fields "first_name", "last_name, and "employee_id" to track Sales-level employees at our company.

- A Customer model with the fields "first_name", "last_name", "address", and "phone_number". to track Customer Leads for the Sales Team.

 - An AutomobileVO model that contains "vin" and "sold" fields to keep track of the Automobiles in our Inventory at all times, via a poller.

 - Lastly, we have a Sale model with the fields "automobile", "salesperson", "customer", and "price". All the fields except for "price" in this model are foreign key fields:

"automobile" is a foreign key to the AutomobileVO model.

 "salesperson" is a foreign key to the Salesperson model.

 "customer" is a foreign key to the Customer model.

 This relationship allows the Sale Model to pull from existing data to track Sales Records that are associated with the Automobile VIN that was sold, the Salespersonw who made the sale, and the Customer who made the purchase.

 We created a polling function to poll for "vin" and "sold" data from the Autombobile Model in the Inventroy microservice every 60 seconds so that objects of the AutomobileVO in the Sales microservice stay synced with objects of the Inventory Microservice at all times.


## Automobile Service Microservice

Our Automobile Service microservice consists of three models.

- A Technician model containing the fields "first_name", "last_name", and "employee_id" to keep track of our talented Service Technicians here at FleetFolio.

- An Appointment model containing the fields "date_time", "reason", "status", "vin", "customer", and "technician" to help us stay on top of appointment management in our Service department.

An AutomobileVO model tracks the "vin" and "sold" fields of Automobile objects in our Inventory so that our AutomobileVOs (Automobile Value Objects) in the Service microservice stay synced with our Inventory at all times.

This is accomplished via a another poller function that polls for "vin" and "sold" data from the Autombobile Model in the Inventory microservice every 60 seconds so that objects of the AutomobileVO in the Service microservice stay synced with objects of the Inventory Microservice at all times.


# Front-End URLs
| URL                                         | Description                      |
|---------------------------------------------|----------------------------------|
| http://localhost:3000/                      | Homepage                          |
| http://localhost:3000/manufacturers         | List Of Manufacturers              |
| http://localhost:3000/manufacturers/create  | Create A Manufacturer              |
| http://localhost:3000/models               | List Of Vehicle Models           |
| http://localhost:3000/salespeople          | List Of Salespeople                |
| http://localhost:3000/salespeople/create   | Create A Salesperson              |
| http://localhost:3000/sales                | List Of Sales Records              |
| http://localhost:3000/sales/create         | Create A Sales Record             |
| http://localhost:3000/sales/history        | Sales History By Salesperson      |
| http://localhost:3000/customers             | List Of Customers                   |
| http://localhost:3000/customers/create      | Create A Customer                |



# Back-End API Documentation

## Inventory - Port 8100
### *Automobile Manufacturers*

From Insomnia and your browser, you can access the manufacturer endpoints at the following URLs.

| Action | Method | URL |
| --- | --- | --- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/ |
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/:id/ |
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/:id/ |
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/:id/ |

##
#### Using Insomnia To Generate Manufacturer Data:


Creating and updating a manufacturer requires only the manufacturer's name.

```json
{
  "name": "Chrysler"
}
```

The return value of creating, getting, and updating a single manufacturer is its name, href, and id.

```json
{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Chrysler"
}

```

The list of manufacturers is a dictionary with the key "manufacturers" set to a list of manufacturers.

```json
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}
```


### *Automobile Models*

From Insomnia and your browser, you can access the vehicle model endpoints at the following URLs.

| Action | Method | URL |
| --- | --- | --- |
| List vehicle models | GET | http://localhost:8100/api/models/ |
| Create a vehicle model | POST | http://localhost:8100/api/models/ |
| Get a specific vehicle model | GET | http://localhost:8100/api/models/:id/ |
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/:id/ |
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/:id/ |

##
#### Using Insomnia To Genrate Automobile Model Data:

Creating a vehicle model requires the model name, a URL of an image, and the id of the manufacturer.

```json
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}

```

Updating a vehicle model can take the name and/or the picture URL. It is not possible to update a vehicle model's manufacturer.

```json
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}

```

Getting the detail of a vehicle model, or the return value from creating or updating a vehicle model, returns the model's information **and** the manufacturer's information.

```json
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}

```

Getting a list of vehicle models returns a list of the detail information with the key "models".

```json
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}

```


## *Automobiles*

From Insomnia and your browser, you can access the automobile endpoints at the following URLs.

**Note**: The identifiers for automobiles in this API are **not** integer ids. They are the Vehicle Identification Number (VIN) for the specific automobile.

| Action | Method | URL |
| --- | --- | --- |
| List automobiles | GET | http://localhost:8100/api/automobiles/ |
| Create an automobile | POST | http://localhost:8100/api/automobiles/ |
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/:vin/ |
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/:vin/ |
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/:vin/ |

##
#### Using Insomnia To Generate Automobile Data:

You can create an automobile with its color, year, VIN, and the id of the vehicle model.

```json
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}

```

As noted, you query an automobile by its VIN. For example, you would use the URL

`http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/`

to get the details for the car with the VIN "1C3CC5FB2AN120174". The details for an automobile include its model and manufacturer.

```json
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  },
  "sold": false
}

```

You can update the color, year, and sold status of an automobile.

```json
{
  "color": "red",
  "year": 2012,
  "sold": true
}

```

Getting a list of automobiles returns a dictionary with the key "autos" set to a list of automobile information.

```json
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      },
      "sold": false
    }
  ]
}
```


## Sales - Port 8090


### *Salespeople*
| Action | Method | URL |
| --- | --- | --- |
| List salespeople | GET | http://localhost:8090/api/salespeople/ |
| Create a salesperson | POST | http://localhost:8090/api/salespeople/ |
| Delete a specific salesperson | DELETE | http://localhost:8090/api/salespeople/:id/ |

#### Using Insomnia To Generate Salespeople Data:

You can create a Salesperson in Insomnia by sending a JSON coded POST request to the above API Endpoint in this format:
```
{
	"first_name": "Vanesssa",
	"last_name": "Pedersen",
	"employee_id": 2950
}
```
### *Customers*
| Action | Method | URL |
| --- | --- | --- |
| List customers | GET | http://localhost:8090/api/customers/ |
| Create a customer | POST | http://localhost:8090/api/customers/ |
| Delete a specific customer | DELETE | http://localhost:8090/api/customers/:id/ |

#### Using Insomnia To Generate Customer Data:

You can create a Customer in Insomnia by sending a JSON coded POST request to the above API Endpoint in this format:
```
{
			"first_name": "Crystal",
			"last_name": "Lake",
			"address": "101 Fake Address Drive, Colorado, 55555",
			"phone_number": "5550101"
}
```

### *Sales Records*
| Action | Method | URL |
| --- | --- | --- |
| List sales | GET | http://localhost:8090/api/sales/ |
| Create a sale | POST | http://localhost:8090/api/sales/ |
| Delete a sale | DELETE | http://localhost:8090/api/sales/:id |

#### Using Insomnia To Generate Sales Record Data:

You can create a record of a Sale in Insomnia but you must use the VIN of an existing automobile, the ID of an existing Salesperson and the ID of an existing Customer.

```
{
	"automobile": "1HGCM82633A123456",
	"salesperson": "1",
	"customer": "1",
	"price":"756000"
}
```

## Service - Port 8080
### *Technicians*
| Action | Method | URL |
| --- | --- | --- |
| List technicians | GET | http://localhost:8080/api/technicians/ |
| Create a technician | POST | http://localhost:8080/api/technicians/ |
| Delete a specific technician | DELETE | http://localhost:8080/api/technicians/:id/ |
### *Appointments*
| Action | Method | URL |
| --- | --- | --- |
| List appointments | GET | http://localhost:8080/api/appointments/ |
| Create an appointment | POST | http://localhost:8080/api/appointments/ |
| Delete an appointment | DELETE | http://localhost:8080/api/appointments/:id/ |
| Set appointment status to "canceled" | PUT | http://localhost:8080/api/appointments/:id/cancel/ |
| Set appointment status to "finished" | PUT | http://localhost:8080/api/appointments/:id/finish/ |

## Diagram(s)
Super rough working diagram of the Inventory and Sales microservices and the models within them.

![Super rough working diagram of the Inventory and Sales microservices and models within them.](Angel-Gevork-Diagram.jpg)
